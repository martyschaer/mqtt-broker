package dev.schaer.mqtt.util;

import java.nio.charset.StandardCharsets;

public class LengthPrefixedData {
    private final byte[] data;
    private final int length;
    private final int newPtr;
    private String string;

    private LengthPrefixedData(final byte[] raw, final int oldPtr) {
        this.length = raw.length;
        this.newPtr = oldPtr + 2 + raw.length;
        this.data = raw;
    }

    /**
     * @return the data at the given pointer as a byte array.
     */
    public static LengthPrefixedData read(final byte[] rawBytes, final int ptr) {
        int length = TwoByteInt.read(rawBytes, ptr);

        byte[] dest = new byte[length];
        System.arraycopy(rawBytes, 2 + ptr, dest, 0, length);
        return new LengthPrefixedData(dest, ptr);
    }

    /**
     * Writes the given string in the LengthPrefixed format and returns the new ptr.
     */
    public static int write(final byte[] rawBytes, final int ptr, final String string) {
        return write(rawBytes, ptr, string.getBytes(StandardCharsets.UTF_8));
    }

    /**
     * Writes the given data in the LengthPrefixed format and returns the new ptr.
     */
    public static int write(final byte[] rawBytes, final int ptr, final byte[] data) {
        TwoByteInt.write(rawBytes, ptr, data.length);
        System.arraycopy(data, 0, rawBytes, ptr + 2, data.length);
        return ptr + 2 + data.length;
    }

    /**
     * @return the length of the string in UTF-8 bytes.
     */
    public int getLength() {
        return length;
    }

    /**
     * @return the pointer used to read this string
     * + 2 byte for the length indicator
     * + the length of the string in UTF-8 bytes.
     */
    public int getNewPtr() {
        return newPtr;
    }

    public String getString() {
        if (string == null) {
            string = new String(data, StandardCharsets.UTF_8);
        }
        return string;
    }

    public byte[] getData() {
        return data;
    }
}
