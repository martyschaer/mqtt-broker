package dev.schaer.mqtt.util;

public final class TwoByteInt {
    private TwoByteInt() {
        // Singleton
    }

    /**
     * Reads the two bytes following ptr in the given rawBytes,
     * and interprets them as an integer in the order MSB LSB.
     */
    public static int read(final byte[] rawBytes, final int ptr) {
        int number = 0;
        number |= (0xFF & rawBytes[ptr]); // ___MSB
        number <<= 8; // shift so it's MSB___
        number |= (0xFF & rawBytes[1 + ptr]); // add LSB: MSBLSB
        return number;
    }

    public static void write(final byte[] rawBytes, final int ptr, final int toWrite) {
        if (toWrite > 0xFFFF) {
            throw new IllegalArgumentException(toWrite + " is larger than 0xFFFF and won't fit.");
        }
        byte msb = (byte) ((toWrite >> 8) & 0xF);
        byte lsb = (byte) (toWrite & 0xF);
        rawBytes[ptr] = msb;
        rawBytes[ptr + 1] = lsb;
    }
}
