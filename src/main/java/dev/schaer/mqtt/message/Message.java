package dev.schaer.mqtt.message;

import dev.schaer.mqtt.packets.PublishPacket;

public class Message {
    private final PublishPacket packet;

    public Message(final PublishPacket packet) {
        this.packet = packet;
    }
}
