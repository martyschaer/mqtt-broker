package dev.schaer.mqtt.topic;

public final class TopicMatcher {
    private static final String SINGLE_LEVEL_WILDCARD = "+";  // U+002B
    private static final String MULTI_LEVEL_WILDCARD = "#";   // U+0023
    private static final String SYS_PREFIX = "$";             // U+0024

    private TopicMatcher() {
        // static singleton
    }

    public static boolean matches(final String filterString, final String topicString) {
        if (topicString.startsWith(SYS_PREFIX) &&
                (filterString.startsWith(MULTI_LEVEL_WILDCARD) || filterString.startsWith(SINGLE_LEVEL_WILDCARD))) {
            // should not match system topics with wildcards
            return false;
        }

        if (filterString.equals(MULTI_LEVEL_WILDCARD)) {
            // a single '#' matches everything
            return true;
        }

        if (!TopicValidator.isValidFilter(filterString)) {
            throw new InvalidTopicFilterException(filterString);
        }

        String regex = filterString.replace("/#", ".*").replace("+", "[^/]*");
        return topicString.matches(regex);
    }
}
