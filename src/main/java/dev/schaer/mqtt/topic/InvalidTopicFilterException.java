package dev.schaer.mqtt.topic;

public class InvalidTopicFilterException extends RuntimeException {
    public InvalidTopicFilterException(String filter) {
        super("'" + filter + "' is an invalid topic filter");
    }
}
