package dev.schaer.mqtt.topic;

public class TopicValidator {
    public static boolean isValidFilter(final String filterString) {
        return !((filterString.contains("#") ^ filterString.endsWith("#")) // '#' must be at the end of the filter
                || filterString.matches("^.*([^/][+#]|\\+[^/]+|\\+/+)$")); // '+' and '#' must be their own level
    }
}
