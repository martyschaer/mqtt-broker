package dev.schaer.mqtt.packets;

import dev.schaer.mqtt.packets.enums.QoS;
import dev.schaer.mqtt.packets.enums.Type;
import dev.schaer.mqtt.util.LengthPrefixedData;
import dev.schaer.mqtt.util.TwoByteInt;

import java.util.ArrayList;
import java.util.List;

public class SubscribePacket extends Packet {
    private final int packetIdentifier;
    private final List<String> topicFilters;
    private final List<QoS> requestedQoS;

    public SubscribePacket(final int length, final int packetIdentifier, final List<String> topicFilters, final List<QoS> requestedQoS) {
        super(length);
        this.packetIdentifier = packetIdentifier;
        this.topicFilters = topicFilters;
        this.requestedQoS = requestedQoS;
    }

    @Override
    public Type getType() {
        return Type.SUBSCRIBE;
    }

    @Override
    public byte[] serialize() {
        throw new UnsupportedOperationException("Not implemented yet!");
    }

    public int getPacketIdentifier() {
        return packetIdentifier;
    }

    public List<String> getTopicFilters() {
        return topicFilters;
    }

    public List<QoS> getRequestedQoS() {
        return requestedQoS;
    }

    public static SubscribePacket parse(final byte flags, final byte[] rawBytes) {
        assert (flags & 0x0F) == 0;

        int ptr = 0;

        SubscribePacketBuilder builder = new SubscribePacketBuilder();
        builder.length(rawBytes.length);

        builder.packetIdentifier(TwoByteInt.read(rawBytes, ptr));
        ptr += 2;

        do {
            LengthPrefixedData topicFilter = LengthPrefixedData.read(rawBytes, ptr);
            ptr = topicFilter.getNewPtr();
            builder.topicFilter(topicFilter.getString());

            byte rawQoS = rawBytes[ptr++];
            builder.requestedQoS(QoS.parseSubQoS(rawQoS));

        } while (ptr < builder.length);

        return builder.build();
    }

    public static class SubscribePacketBuilder extends PacketBuilder<SubscribePacket> {
        private int packetIdentifier;
        private List<String> topicFilters = new ArrayList<>();
        private List<QoS> requestedQoS = new ArrayList<>();

        public SubscribePacketBuilder packetIdentifier(final int packetIdentifier) {
            this.packetIdentifier = packetIdentifier;
            return this;
        }

        public SubscribePacketBuilder topicFilter(final String topicFilter) {
            topicFilters.add(topicFilter);
            return this;
        }

        public SubscribePacketBuilder requestedQoS(final QoS qos) {
            requestedQoS.add(qos);
            return this;
        }

        @Override
        public SubscribePacket build() {
            return new SubscribePacket(
                    length,
                    packetIdentifier,
                    topicFilters,
                    requestedQoS
            );
        }
    }
}
