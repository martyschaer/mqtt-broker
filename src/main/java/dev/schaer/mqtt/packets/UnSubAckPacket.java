package dev.schaer.mqtt.packets;

import dev.schaer.mqtt.packets.enums.Type;
import dev.schaer.mqtt.util.TwoByteInt;

public class UnSubAckPacket extends Packet {
    private final int packetIdentifier;

    public UnSubAckPacket(final int length, final int packetIdentifier) {
        super(length);
        this.packetIdentifier = packetIdentifier;
    }

    public int getPacketIdentifier() {
        return packetIdentifier;
    }

    @Override
    public Type getType() {
        return Type.UNSUBACK;
    }

    @Override
    public byte[] serialize() {
        byte[] remainingLengthSerialized = LengthEncoding.encode(getLength());
        byte[] serialized = new byte[1 + remainingLengthSerialized.length + getLength()]; // fixed header

        int ptr = 0;

        // Packet Type
        serialized[ptr++] = getType().getKey();

        // Remaining Length
        for (int i = 0; i < remainingLengthSerialized.length; i++) {
            serialized[ptr++] = remainingLengthSerialized[i];
        }

        // Variable Header
        TwoByteInt.write(serialized, ptr, packetIdentifier);
        ptr += 2;

        return serialized;
    }

    public static class UnSubAckPacketBuilder extends PacketBuilder<UnSubAckPacket> {
        private int packetIdentifier;

        public UnSubAckPacketBuilder packetIdentifier(final int packetIdentifier) {
            this.packetIdentifier = packetIdentifier;
            return this;
        }

        @Override
        public UnSubAckPacket build() {
            return new UnSubAckPacket(
                    // variable header
                    2,
                    packetIdentifier
            );
        }
    }
}
