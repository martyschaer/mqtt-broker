package dev.schaer.mqtt.packets;

import dev.schaer.mqtt.packets.enums.Type;
import dev.schaer.mqtt.util.LengthPrefixedData;
import dev.schaer.mqtt.util.TwoByteInt;

import java.util.ArrayList;
import java.util.List;

public class UnsubscribePacket extends Packet {
    private final int packetIdentifier;
    private final List<String> topicFilters;

    public UnsubscribePacket(final int length, final int packetIdentifier, final List<String> topicFilters) {
        super(length);
        this.packetIdentifier = packetIdentifier;
        this.topicFilters = topicFilters;
    }

    @Override
    public Type getType() {
        return Type.UNSUBSCRIBE;
    }

    @Override
    public byte[] serialize() {
        throw new UnsupportedOperationException("Not implemented yet!");
    }

    public int getPacketIdentifier() {
        return packetIdentifier;
    }

    public List<String> getTopicFilters() {
        return topicFilters;
    }

    public static UnsubscribePacket parse(final byte flags, final byte[] rawBytes) {
        assert (flags & 0x0F) == 2; // flags are reserved for future use

        int ptr = 0;

        UnsubscribePacketBuilder builder = new UnsubscribePacketBuilder();

        // Remaining Length
        builder.length(rawBytes.length);

        // Packet Identifier
        builder.packetIdentifier(TwoByteInt.read(rawBytes, ptr));
        ptr += 2;

        do {
            // The topics to unsubscribe from
            LengthPrefixedData topicFilter = LengthPrefixedData.read(rawBytes, ptr);
            ptr = topicFilter.getNewPtr();
            builder.topicFilter(topicFilter.getString());
        } while (ptr < builder.length);

        return builder.build();
    }

    public static class UnsubscribePacketBuilder extends PacketBuilder<UnsubscribePacket> {
        private int packetIdentifier;
        private List<String> topicFilters = new ArrayList<>();

        public UnsubscribePacketBuilder packetIdentifier(final int packetIdentifier) {
            this.packetIdentifier = packetIdentifier;
            return this;
        }

        public UnsubscribePacketBuilder topicFilter(final String topicFilter) {
            topicFilters.add(topicFilter);
            return this;
        }

        @Override
        public UnsubscribePacket build() {
            return new UnsubscribePacket(
                    length,
                    packetIdentifier,
                    topicFilters
            );
        }
    }
}
