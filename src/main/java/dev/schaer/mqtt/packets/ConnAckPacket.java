package dev.schaer.mqtt.packets;

import dev.schaer.mqtt.packets.enums.ReturnCode;
import dev.schaer.mqtt.packets.enums.Type;

public class ConnAckPacket extends Packet {
    private final boolean sessionPresent;
    private final ReturnCode returnCode;

    public ConnAckPacket(final boolean sessionPresent, final ReturnCode returnCode) {
        super(2);
        this.sessionPresent = sessionPresent;
        this.returnCode = returnCode;
    }

    @Override
    public Type getType() {
        return Type.CONNACK;
    }

    @Override
    public byte[] serialize() {
        return new byte[]{
                getType().getKey(),
                0x02, // length is always 2 for a CONNACK packet
                (byte) (sessionPresent ? 0x01 : 0x00),
                returnCode.getCode()
        };
    }

    public static class ConnAckPacketBuilder extends PacketBuilder<ConnAckPacket> {
        private boolean sessionPresent;
        private ReturnCode returnCode;

        public ConnAckPacketBuilder sessionPresent(final boolean sessionPresent) {
            this.sessionPresent = sessionPresent;
            return this;
        }

        public ConnAckPacketBuilder returnCode(final ReturnCode returnCode) {
            this.returnCode = returnCode;
            return this;
        }

        @Override
        public ConnAckPacket build() {
            return new ConnAckPacket(
                    sessionPresent,
                    returnCode
            );
        }
    }
}
