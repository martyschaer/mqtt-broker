package dev.schaer.mqtt.packets;

import dev.schaer.mqtt.packets.enums.Protocol;
import dev.schaer.mqtt.packets.enums.QoS;
import dev.schaer.mqtt.packets.enums.Type;
import dev.schaer.mqtt.util.LengthPrefixedData;
import dev.schaer.mqtt.util.TwoByteInt;

import java.util.Arrays;

public class ConnectPacket extends Packet {
    private static final Type TYPE = Type.CONNECT;

    private final boolean userNameSet;
    private final boolean passwordSet;
    private final boolean willRetain;
    private final QoS willQoS;
    private final boolean willSet;
    private final boolean cleanSession;
    private final int keepAlive;
    private final String clientId;
    private final String willTopic;
    private final byte[] willMessage;
    private final String userName;
    private final byte[] password;

    private ConnectPacket(int length, boolean userNameSet, boolean passwordSet, boolean willRetain, QoS willQoS, boolean willSet, boolean cleanSession, int keepAlive, String clientId, String willTopic, byte[] willMessage, String userName, byte[] password) {
        super(length);
        this.userNameSet = userNameSet;
        this.passwordSet = passwordSet;
        this.willRetain = willRetain;
        this.willQoS = willQoS;
        this.willSet = willSet;
        this.cleanSession = cleanSession;
        this.keepAlive = keepAlive;
        this.clientId = clientId;
        this.willTopic = willTopic;
        this.willMessage = willMessage;
        this.userName = userName;
        this.password = password;
    }

    public boolean isUserNameSet() {
        return userNameSet;
    }

    public boolean isPasswordSet() {
        return passwordSet;
    }

    public boolean isWillRetain() {
        return willRetain;
    }

    public QoS getWillQoS() {
        return willQoS;
    }

    public boolean isWillSet() {
        return willSet;
    }

    public boolean isCleanSession() {
        return cleanSession;
    }

    public int getKeepAlive() {
        return keepAlive;
    }

    public String getClientId() {
        return clientId;
    }

    public String getWillTopic() {
        return willTopic;
    }

    public byte[] getWillMessage() {
        return willMessage;
    }

    public String getUserName() {
        return userName;
    }

    public byte[] getPassword() {
        return password;
    }

    public static ConnectPacket parse(final byte flags, final byte[] rawBytes) {
        int ptr = 0;

        LengthPrefixedData protocolName = LengthPrefixedData.read(rawBytes, ptr);
        ptr = protocolName.getNewPtr();
        assert protocolName.getString().equals(Protocol.NAME);
        Protocol protocolLevel = Protocol.parse(rawBytes[ptr++]);
        assert protocolLevel == Protocol.v3_1_1;

        ConnectPacketBuilder builder = new ConnectPacketBuilder();
        parseFlags(builder, rawBytes[ptr++]);

        builder.length(rawBytes.length);

        builder.keepAlive(TwoByteInt.read(rawBytes, ptr));
        ptr += 2;

        LengthPrefixedData clientId = LengthPrefixedData.read(rawBytes, ptr);
        ptr = clientId.getNewPtr();
        builder.clientId(clientId.getString());

        if (builder.willSet) {
            LengthPrefixedData willTopic = LengthPrefixedData.read(rawBytes, ptr);
            ptr = willTopic.getNewPtr();
            builder.willTopic(willTopic.getString());

            LengthPrefixedData willMessage = LengthPrefixedData.read(rawBytes, ptr);
            ptr = willMessage.getNewPtr();
            builder.willMessage(willMessage.getData());
        }

        if (builder.userNameSet) {
            LengthPrefixedData userName = LengthPrefixedData.read(rawBytes, ptr);
            ptr = userName.getNewPtr();
            builder.userName(userName.getString());
        }

        if (builder.passwordSet) {
            LengthPrefixedData password = LengthPrefixedData.read(rawBytes, ptr);
            ptr = password.getNewPtr();
            builder.password(password.getData());
        }

        return builder.build();
    }

    private static void parseFlags(final ConnectPacketBuilder builder, final byte flags) {
        assert (flags & 0x01) == 0; // check that the reserved flag is respected
        builder.userNameSet((flags & 0x80) == 0x80);
        builder.passwordSet((flags & 0x40) == 0x40);
        builder.willRetained((flags & 0x20) == 0x20);
        builder.willSet((flags & 0x04) == 0x04);
        builder.cleanSession((flags & 0x02) == 0x02);
        builder.willQoS(QoS.parseConnectFlags(flags)); // 0x10 and 0x08 determine the QoS

        if (builder.willSet) {
            assert builder.willQoS == QoS.AT_MOST_ONCE;
            assert !builder.willRetained;
        }
    }

    @Override
    public Type getType() {
        return TYPE;
    }

    @Override
    public byte[] serialize() {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public String toString() {
        return "ConnectPacket{" +
                super.toString() +
                "userNameSet=" + userNameSet +
                ", passwordSet=" + passwordSet +
                ", willRetain=" + willRetain +
                ", willQoS=" + willQoS +
                ", willSet=" + willSet +
                ", cleanSession=" + cleanSession +
                ", keepAlive=" + keepAlive +
                ", clientId='" + clientId + '\'' +
                ", willTopic='" + willTopic + '\'' +
                ", willMessage=" + Arrays.toString(willMessage) +
                ", userName='" + userName + '\'' +
                ", password=" + Arrays.toString(password) +
                '}';
    }

    public static class ConnectPacketBuilder extends PacketBuilder<ConnectPacket> {
        private boolean userNameSet;
        private boolean passwordSet;
        private boolean willRetained;
        private QoS willQoS;
        private boolean willSet;
        private boolean cleanSession;
        private int keepAlive;
        private String clientId;
        private String willTopic;
        private byte[] willMessage;
        private String userName;
        private byte[] password;

        private ConnectPacketBuilder userNameSet(final boolean userNameSet) {
            this.userNameSet = userNameSet;
            return this;
        }

        private ConnectPacketBuilder passwordSet(final boolean passwordSet) {
            this.passwordSet = passwordSet;
            return this;
        }

        private ConnectPacketBuilder willRetained(final boolean willRetain) {
            this.willRetained = willRetain;
            return this;
        }

        private ConnectPacketBuilder willQoS(final QoS willQoS) {
            this.willQoS = willQoS;
            return this;
        }

        private ConnectPacketBuilder willSet(final boolean willSet) {
            this.willSet = willSet;
            return this;
        }

        private ConnectPacketBuilder cleanSession(final boolean cleanSession) {
            this.cleanSession = cleanSession;
            return this;
        }

        private ConnectPacketBuilder keepAlive(final int keepAlive) {
            this.keepAlive = keepAlive;
            return this;
        }

        private ConnectPacketBuilder clientId(final String clientId) {
            this.clientId = clientId;
            return this;
        }

        private ConnectPacketBuilder willTopic(final String willTopic) {
            this.willTopic = willTopic;
            return this;
        }

        private ConnectPacketBuilder willMessage(final byte[] willMessage) {
            this.willMessage = willMessage;
            return this;
        }

        private ConnectPacketBuilder userName(final String userName) {
            this.userName = userName;
            return this;
        }

        private ConnectPacketBuilder password(final byte[] password) {
            this.password = password;
            return this;
        }

        @Override
        public ConnectPacket build() {
            return new ConnectPacket(
                    length,
                    userNameSet,
                    passwordSet,
                    willRetained,
                    willQoS,
                    willSet,
                    cleanSession,
                    keepAlive,
                    clientId,
                    willTopic,
                    willMessage,
                    userName,
                    password
            );
        }
    }

}
