package dev.schaer.mqtt.packets;

import dev.schaer.mqtt.packets.enums.Type;

public class PingReqPacket extends Packet {
    public PingReqPacket() {
        super(0); // there is no variable header and not payload
    }

    @Override
    public Type getType() {
        return Type.PINGREQ;
    }

    @Override
    public byte[] serialize() {
        return new byte[]{
                getType().getKey(),
                0x00 // remaining length
        };
    }

    public static PingReqPacket parse(final byte flags, final byte[] rawBytes) {
        assert (flags & 0x0F) == 0;
        assert rawBytes.length == 0;
        return new PingReqPacket();
    }
}
