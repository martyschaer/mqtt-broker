package dev.schaer.mqtt.packets;

import dev.schaer.mqtt.packets.enums.Type;

import java.util.HashMap;
import java.util.Map;
import java.util.function.BiFunction;

public final class PacketParser {
    private static final Map<Type, BiFunction<Byte, byte[], Packet>> PARSERS = new HashMap<>();

    static {
        PARSERS.put(Type.CONNECT, ConnectPacket::parse);
        PARSERS.put(Type.SUBSCRIBE, SubscribePacket::parse);
        PARSERS.put(Type.PUBLISH, PublishPacket::parse);
        PARSERS.put(Type.PINGREQ, PingReqPacket::parse);
        PARSERS.put(Type.PINGRESP, PingRespPacket::parse);
    }

    private PacketParser() {
        // Singleton
    }

    public static Packet parse(final Type type, final byte flags, final byte[] rawBytes) {
        if (!PARSERS.containsKey(type)) {
            throw new UnsupportedOperationException(type.name() + " is not supported yet!");
        }
        return PARSERS.get(type).apply(flags, rawBytes);
    }
}
