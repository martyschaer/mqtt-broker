package dev.schaer.mqtt.packets;

import dev.schaer.mqtt.packets.enums.QoS;
import dev.schaer.mqtt.packets.enums.Type;
import dev.schaer.mqtt.util.TwoByteInt;

import java.util.ArrayList;
import java.util.List;

public class SubAckPacket extends Packet {
    private final int packetIdentifier;
    private final List<QoS> grantedQos;

    public SubAckPacket(final int length, final int packetIdentifier, final List<QoS> grantedQos) {
        super(length);
        this.packetIdentifier = packetIdentifier;
        this.grantedQos = grantedQos;
    }

    public int getPacketIdentifier() {
        return packetIdentifier;
    }

    @Override
    public Type getType() {
        return Type.SUBACK;
    }

    @Override
    public byte[] serialize() {
        byte[] remainingLengthSerialized = LengthEncoding.encode(getLength());
        byte[] serialized = new byte[1 + remainingLengthSerialized.length + getLength()]; // fixed header

        int ptr = 0;

        // Packet Type
        serialized[ptr++] = getType().getKey();

        // Remaining Length
        for (int i = 0; i < remainingLengthSerialized.length; i++) {
            serialized[ptr++] = remainingLengthSerialized[i];
        }

        // Variable Header
        TwoByteInt.write(serialized, ptr, packetIdentifier);
        ptr += 2;

        // Payload
        for (QoS granted : grantedQos) {
            serialized[ptr++] = (byte) (granted == null ? 0x80 : granted.ordinal());
        }

        return serialized;
    }

    public static class SubAckPacketBuilder extends PacketBuilder<SubAckPacket> {
        private int packetIdentifier;
        private List<QoS> grantedQoS = new ArrayList<>();

        public SubAckPacketBuilder packetIdentifier(final int packetIdentifier) {
            this.packetIdentifier = packetIdentifier;
            return this;
        }

        public SubAckPacketBuilder grantedQoS(final QoS qos) {
            this.grantedQoS.add(qos);
            return this;
        }

        @Override
        public SubAckPacket build() {
            return new SubAckPacket(
                    // variable header + one byte per ACK
                    2 + grantedQoS.size(),
                    packetIdentifier,
                    grantedQoS
            );
        }
    }
}
