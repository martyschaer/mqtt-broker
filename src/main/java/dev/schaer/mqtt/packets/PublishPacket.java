package dev.schaer.mqtt.packets;

import dev.schaer.mqtt.packets.enums.QoS;
import dev.schaer.mqtt.packets.enums.Type;
import dev.schaer.mqtt.util.LengthPrefixedData;
import dev.schaer.mqtt.util.TwoByteInt;

public class PublishPacket extends Packet {

    private final String topic;
    private final QoS qos;
    private final int packetIdentifier;
    private final byte[] payload;

    public PublishPacket(int length, String topic, QoS qos, int packetIdentifier, byte[] payload) {
        super(length);
        this.topic = topic;
        this.qos = qos;
        this.packetIdentifier = packetIdentifier;
        this.payload = payload;
    }

    @Override
    public Type getType() {
        return Type.PUBLISH;
    }

    @Override
    public byte[] serialize() {
        byte[] remainingLengthSerialized = LengthEncoding.encode(getLength());
        byte[] serialized = new byte[1 + remainingLengthSerialized.length + getLength()]; // fixed header

        int ptr = 0;

        // TODO extract some of this into the generic Packet

        // Packet Type
        serialized[ptr++] = (byte) ((getType().getKey() | (qos.ordinal() << 1)));

        // Remaining Length
        for (int i = 0; i < remainingLengthSerialized.length; i++) {
            serialized[ptr++] = remainingLengthSerialized[i];
        }

        // Variable Header
        // Topic
        ptr = LengthPrefixedData.write(serialized, ptr, topic);

        // Packet Identifier
        if (qos.ordinal() > QoS.AT_MOST_ONCE.ordinal()) {
            TwoByteInt.write(serialized, ptr, packetIdentifier);
            ptr += 2;
        }

        // Payload
        System.arraycopy(payload, 0, serialized, ptr, payload.length);
        return serialized;
    }

    public String getTopic() {
        return topic;
    }

    public QoS getQos() {
        return qos;
    }

    public int getPacketIdentifier() {
        return packetIdentifier;
    }

    public byte[] getPayload() {
        return payload;
    }

    public static PublishPacket parse(final byte flags, final byte[] rawBytes) {
        assert (flags & 0x8) == 0; // TODO handle DUP flag correctly
        assert (flags & 0x1) == 0; // TODO handle RETAIN flag correctly

        PublishPacketBuilder builder = new PublishPacketBuilder();
        int ptr = 0;

        builder.length = rawBytes.length;

        // QoS
        builder.qos(QoS.parsePublishFlags(flags));

        // Topic
        LengthPrefixedData topic = LengthPrefixedData.read(rawBytes, ptr);
        ptr = topic.getNewPtr();
        builder.topic(topic.getString());

        if (builder.qos.ordinal() > QoS.AT_MOST_ONCE.ordinal()) {
            // Packet Identifier
            builder.packetIdentifier(TwoByteInt.read(rawBytes, ptr));
            ptr += 2;
        }

        byte[] payload = new byte[rawBytes.length - ptr];
        System.arraycopy(rawBytes, ptr, payload, 0, payload.length);
        builder.payload(payload);

        return builder.build();
    }

    public static class PublishPacketBuilder extends PacketBuilder<PublishPacket> {
        private String topic;
        private QoS qos;
        private int packetIdentifier;
        private byte[] payload;

        public PublishPacketBuilder topic(final String topic) {
            this.topic = topic;
            return this;
        }

        public PublishPacketBuilder qos(final QoS qos) {
            this.qos = qos;
            return this;
        }

        public PublishPacketBuilder packetIdentifier(final int packetIdentifier) {
            this.packetIdentifier = packetIdentifier;
            return this;
        }

        public PublishPacketBuilder payload(final byte[] payload) {
            this.payload = payload;
            return this;
        }

        @Override
        public PublishPacket build() {
            return new PublishPacket(
                    length,
                    topic,
                    qos,
                    packetIdentifier,
                    payload
            );
        }
    }
}
