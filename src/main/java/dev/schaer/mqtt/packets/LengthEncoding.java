package dev.schaer.mqtt.packets;

import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedList;
import java.util.List;

public final class LengthEncoding {
    private static final int MAX_ENCODING_LENGTH = 4;
    private static final int MAX_LENGTH = 268_435_455;
    private static final byte CONTINUATION_MARKER = (byte) 0x80;

    private LengthEncoding() {
        // Singleton
    }

    public static byte[] encode(final int toEncode) {
        if (toEncode > MAX_LENGTH) {
            throw new IllegalArgumentException(toEncode + " is too large to be encoded.");
        }

        int x = toEncode;
        List<Byte> bytes = new LinkedList<>();
        do {
            byte encodedByte = (byte) (x % 0x80);
            x = x / 0x80;
            if (x > 0) {
                encodedByte |= 0x80;
            }
            bytes.add(encodedByte);
        } while (x > 0);

        byte[] output = new byte[bytes.size()];
        for (int i = 0; i < bytes.size(); i++) {
            output[i] = bytes.get(i);
        }
        return output;
    }

    /**
     * Reads the variable length encoding from the given {@link InputStream}.
     * This reads bytes from the input, but only the bytes which make up the encoded length.
     * Any following bytes are left intact on the input.
     *
     * @throws IOException              if something goes wrong reading from the stream.
     * @throws IllegalArgumentException if the given input doesn't contain a valid encoding.
     */
    public static int decode(final InputStream input) throws IOException {
        int multiplier = 1;
        int value = 0;
        short encodedByte;
        do {
            if ((encodedByte = (short) input.read()) == -1) {
                throw new IllegalStateException("InputStream ended unexpectedly");
            }
            value += (encodedByte & 0x7F) * multiplier;
            if (multiplier > MAX_LENGTH) {
                throw new IllegalArgumentException("Malformed Remaining Length");
            }
            multiplier *= 0x80;
        } while ((encodedByte & CONTINUATION_MARKER) != 0);

        return value;
    }
}
