package dev.schaer.mqtt.packets;

import dev.schaer.mqtt.packets.enums.Type;

/**
 * General structure of a {@link Packet}:
 * <pre>
 * +----------------------------+
 * | Fixed Header               |
 * +----------------------------+
 * | Variable Header (Optional) |
 * +----------------------------+
 * | Payload         (Optional) |
 * +----------------------------+
 * </pre>
 */
public abstract class Packet {
    private final int length;

    public Packet(final int length) {
        this.length = length;
    }

    public int getLength() {
        return length;
    }

    public abstract Type getType();

    public abstract byte[] serialize();

    public static abstract class PacketBuilder<T extends Packet> {
        protected int length;

        public PacketBuilder<T> length(int length) {
            this.length = length;
            return this;
        }

        public abstract T build();
    }

    @Override
    public String toString() {
        return "length=" + length + ", type=" + getType().name() + ", ";
    }
}
