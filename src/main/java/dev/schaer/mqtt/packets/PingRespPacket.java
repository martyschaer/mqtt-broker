package dev.schaer.mqtt.packets;

import dev.schaer.mqtt.packets.enums.Type;

public class PingRespPacket extends Packet {
    public PingRespPacket() {
        super(0); // there is no variable header and not payload
    }

    @Override
    public Type getType() {
        return Type.PINGRESP;
    }

    @Override
    public byte[] serialize() {
        return new byte[]{
                getType().getKey(),
                0x00 // remaining length
        };
    }

    public static PingRespPacket parse(final byte flags, final byte[] rawBytes) {
        assert (flags & 0x0F) == 0;
        assert rawBytes.length == 0;
        return new PingRespPacket();
    }
}
