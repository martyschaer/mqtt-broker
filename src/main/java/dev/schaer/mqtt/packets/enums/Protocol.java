package dev.schaer.mqtt.packets.enums;

public enum Protocol {
    v3_1_1(0x4);

    public static final String NAME = "MQTT";

    private static final int LEVEL_MASK = 0xFF;
    private final int level;

    Protocol(final int level) {
        this.level = level;
    }

    public static Protocol parse(final int protocolVersion) {
        if ((protocolVersion & LEVEL_MASK) == v3_1_1.level) {
            return v3_1_1;
        }
        throw new IllegalArgumentException("Only MQTT v3.1.1 is currently supported.");
    }
}
