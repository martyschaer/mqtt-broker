package dev.schaer.mqtt.packets.enums;

public enum QoS {
    AT_MOST_ONCE,
    AT_LEAST_ONCE,
    EXACTLY_ONCE;

    private static final int CONNECT_QOS_SHIFT = 3;
    private static final int PUBLISH_QOS_SHIFT = 1;

    public static QoS parseSubQoS(final byte flags) {
        return parseShiftedFlags(flags); // already on the LSBits.
    }

    public static QoS parsePublishFlags(final byte flags) {
        return parseShiftedFlags(flags >>> PUBLISH_QOS_SHIFT);
    }

    public static QoS parseConnectFlags(final int flags) {
        return parseShiftedFlags(flags >>> CONNECT_QOS_SHIFT);
    }

    private static QoS parseShiftedFlags(final int flags) {
        switch (flags & 3) { // only last two bits
            case 0:
                return AT_MOST_ONCE;
            case 1:
                return AT_LEAST_ONCE;
            case 2:
                return EXACTLY_ONCE;
        }
        throw new IllegalArgumentException("Only 0, 1, 2 are valid QoS");
    }
}
