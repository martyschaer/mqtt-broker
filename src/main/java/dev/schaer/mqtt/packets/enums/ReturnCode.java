package dev.schaer.mqtt.packets.enums;

public enum ReturnCode {
    ACCEPTED(0x00),
    REFUSED_PROTOCOL_VERSION(0x01),
    REFUSED_IDENTIFIER_REJECTED(0x02),
    REFUSED_SERVER_UNAVAILABLE(0x03),
    REFUSED_BAD_AUTHENTICATION(0x04),
    REFUSED_NOT_AUTHORIZED(0x05);

    private final byte code;

    ReturnCode(int code) {
        this.code = (byte) code;
    }

    public byte getCode() {
        return code;
    }
}
