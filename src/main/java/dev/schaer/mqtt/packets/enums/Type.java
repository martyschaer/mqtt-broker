package dev.schaer.mqtt.packets.enums;

import java.util.HashMap;
import java.util.Map;

public enum Type {
    Reserved1(0x00),
    CONNECT(0x10),
    CONNACK(0x20),
    PUBLISH(0x30),
    PUBACK(0x40),
    PUBREC(0x50),
    PUBREL(0x60),
    PUBCOMP(0x70),
    SUBSCRIBE(0x80),
    SUBACK(0x90),
    UNSUBSCRIBE(0xA0),
    UNSUBACK(0xB0),
    PINGREQ(0xC0),
    PINGRESP(0xD0),
    DISCONNECT(0xE0),
    Reserved2(0xF0);

    private static final int TYPE_MASK = 0xF0;
    private static final Map<Integer, Type> CACHE = new HashMap<>();

    static {
        for (Type t : values()) {
            CACHE.put(t.key, t);
        }
    }

    private final int key;

    Type(final int key) {
        this.key = key;
    }

    public byte getKey() {
        return (byte) key;
    }

    public static Type parse(final int raw) {
        return CACHE.get(raw & TYPE_MASK);
    }
}
