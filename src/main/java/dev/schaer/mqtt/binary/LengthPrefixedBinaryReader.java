package dev.schaer.mqtt.binary;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

/**
 * Reads a number of bytes from the {@link InputStream} in the format
 * [Length-MSB, Length-LSB, data...].
 */
public class LengthPrefixedBinaryReader extends BinaryReader {

    private final TwoByteNumberBinaryReader numberReader;

    public LengthPrefixedBinaryReader(InputStream input) {
        super(input);
        this.numberReader = new TwoByteNumberBinaryReader(input);
    }

    public byte[] read() throws IOException {
        final int length = readLength();
        final byte[] dest = new byte[length];
        assert length == input.read(dest);
        return dest;
    }

    private int readLength() throws IOException {
        return numberReader.read();
    }

    public String readString() throws IOException {
        return new String(read(), StandardCharsets.UTF_8);
    }

}
