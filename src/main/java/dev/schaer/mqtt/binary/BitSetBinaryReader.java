package dev.schaer.mqtt.binary;

import java.io.IOException;
import java.io.InputStream;
import java.util.BitSet;

public class BitSetBinaryReader extends BinaryReader {

    public BitSetBinaryReader(InputStream input) {
        super(input);
    }

    public BitSet read() throws IOException {
        final BitSet result = new BitSet(8);
        final int raw = input.read();
        assert raw != -1;
        int mask = 0x01;
        for (int i = 0; i < 8; i++) {
            if ((raw & mask) != 0) result.set(i);
            mask <<= 1; // shift mask over by one bit
        }
        return result;
    }
}
