package dev.schaer.mqtt.binary;

import java.io.IOException;
import java.io.InputStream;

/**
 * Reads two bytes from the {@link InputStream}, and returns them as an integer.
 * The two bytes are interpreted as [MSB, LSB].
 * <p>
 * Throws an {@link AssertionError} if the end of {@link InputStream} is encountered unexpectedly.
 */
public class TwoByteNumberBinaryReader extends BinaryReader {
    public TwoByteNumberBinaryReader(InputStream input) {
        super(input);
    }

    public int read() throws IOException {
        int length = 0;
        assert -1 != (length |= input.read()); // read most significant bits
        length <<= 8;
        assert -1 != (length |= input.read()); // read least significant bits
        return length;
    }
}
