package dev.schaer.mqtt.binary;

import java.io.InputStream;

public abstract class BinaryReader {

    final InputStream input;

    public BinaryReader(final InputStream input) {
        this.input = input;
    }
}
