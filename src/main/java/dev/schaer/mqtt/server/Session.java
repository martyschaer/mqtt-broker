package dev.schaer.mqtt.server;

import dev.schaer.mqtt.packets.*;
import dev.schaer.mqtt.packets.enums.ReturnCode;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.Marker;
import org.apache.logging.log4j.MarkerManager;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.time.Instant;

public class Session {
    private RawSession rawSession;
    private int keepAlive;
    private boolean cleanSession;
    private final String clientId;
    private Instant lastMessage = Instant.now();
    private Thread receiver;

    private static final Logger logger = LogManager.getLogger(Session.class);
    private final Marker LOG_MARKER;

    public void publish(Packet packet) throws IOException {
        rawSession.send(packet);
        logger.info("[{}] sent packet to client.", clientId);
    }

    public Session(final String clientId, final RawSession rawSession, final boolean cleanSession, final int keepAlive) {
        this.clientId = clientId;
        this.rawSession = rawSession;
        this.cleanSession = cleanSession;
        this.keepAlive = keepAlive;

        LOG_MARKER = MarkerManager.getMarker(clientId);

        receiver = new Thread(new PacketHandler());
        receiver.start();
    }

    private class PacketHandler implements Runnable {

        @Override
        public void run() {
            try {
                finishConnection();
                while (!rawSession.isClosed()) {
                    handlePacket(rawSession.receive());
                }
                SessionStore.onlyInstance().removeSession(Session.this);
                logger.info("[{}] Client closed connection. Removing session.", clientId);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        private void finishConnection() throws IOException {
            ConnAckPacket connAckPacket = new ConnAckPacket.ConnAckPacketBuilder()
                    .returnCode(ReturnCode.ACCEPTED)
                    .sessionPresent(false) // TODO handle cleanSession = 0
                    .build();
            rawSession.send(connAckPacket);
            logger.info(LOG_MARKER, "[{}] Accepted new session.", clientId);
        }

        private void handlePacket(final Packet packet) throws IOException {
            if(packet == null) {
                return;
            }
            lastMessage = Instant.now();
            switch (packet.getType()) {
                case CONNECT -> handleConnect((ConnectPacket) packet);
                case SUBSCRIBE -> handleSubscribe((SubscribePacket) packet);
                case UNSUBSCRIBE -> handleUnsubscribe((UnsubscribePacket) packet);
                case PUBLISH -> handlePublish((PublishPacket) packet);
                case PINGREQ -> handlePingReq((PingReqPacket) packet);
            }
        }

        private void handleConnect(final ConnectPacket packet) {
            if (packet.getClientId().equals(clientId)) {
                keepAlive = packet.getKeepAlive();
                cleanSession = packet.isCleanSession();
            }
        }

        private void handleSubscribe(final SubscribePacket packet) throws IOException {
            SubAckPacket.SubAckPacketBuilder ackBuilder = new SubAckPacket.SubAckPacketBuilder();
            ackBuilder.packetIdentifier(packet.getPacketIdentifier());

            logger.debug("[{}] Received SUBSCRIBE packId={}", clientId, packet.getPacketIdentifier());

            packet.getTopicFilters().forEach(topicFilter -> {
                logger.info("[{}] Added subscription '{}'", clientId, topicFilter);
                ackBuilder.grantedQoS(SessionStore.onlyInstance().addSubscription(topicFilter, clientId));
            });

            rawSession.send(ackBuilder.build());
            logger.debug("[{}] Acknowledged SUBSCRIBE packId={}", clientId, packet.getPacketIdentifier());
        }

        private void handleUnsubscribe(final UnsubscribePacket packet) throws IOException {
            UnSubAckPacket.UnSubAckPacketBuilder ackBuilder = new UnSubAckPacket.UnSubAckPacketBuilder();
            ackBuilder.packetIdentifier(packet.getPacketIdentifier());

            logger.debug("[{}] Received UNSUBSCRIBE packId={}", clientId, packet.getPacketIdentifier());

            packet.getTopicFilters().forEach(topicFilter -> {
                logger.info("[{}] Removed subscription '{}'", clientId, topicFilter);
            });

            rawSession.send(ackBuilder.build());
            logger.debug("[{}] Acknowledged UNSUBSCRIBE packId={}", clientId, packet.getPacketIdentifier());
        }

        private void handlePublish(final PublishPacket packet) {
            logger.info("[{}] Received PUBLISH: topic={}, payload={}", clientId, packet.getTopic(), new String(packet.getPayload(), StandardCharsets.UTF_8));
            SessionStore.onlyInstance().publish(packet);
        }

        private void handlePingReq(final PingReqPacket packet) throws IOException {
            logger.debug("[{}] Received PINGREQ, publishing PINGRESP.", clientId);
            rawSession.send(new PingRespPacket());
        }
    }

    public RawSession getRawSession() {
        return rawSession;
    }

    public void setRawSession(RawSession rawSession) {
        this.rawSession = rawSession;
    }

    public int getKeepAlive() {
        return keepAlive;
    }

    public void setKeepAlive(int keepAlive) {
        this.keepAlive = keepAlive;
    }

    public boolean isCleanSession() {
        return cleanSession;
    }

    public void setCleanSession(boolean cleanSession) {
        this.cleanSession = cleanSession;
    }

    public String getClientId() {
        return clientId;
    }

    public void disconnect() throws IOException {
        rawSession.close();
    }
}
