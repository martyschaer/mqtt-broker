package dev.schaer.mqtt.server;

import dev.schaer.mqtt.packets.ConnectPacket;
import dev.schaer.mqtt.packets.Packet;
import dev.schaer.mqtt.packets.enums.Type;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
    private static final int PORT = 1883;
    private static final Logger logger = LogManager.getLogger(Server.class);

    private static SessionStore sessionStore = SessionStore.onlyInstance();

    public static void main(String[] args) throws IOException {
        ServerSocket listenerSocket = new ServerSocket(PORT);
        while (true) {
            logger.debug("Waiting for connection...");
            Socket connection = listenerSocket.accept();
            logger.info("Connection acquired: {}", connection);

            RawSession session = new RawSession(connection);
            Packet received = session.receive();
            if (Type.CONNECT == received.getType()) {
                ConnectPacket connectPacket = (ConnectPacket) received;
                logger.debug("Received CONNECT: {}", connectPacket.toString());
                sessionStore.addSession(new Session(
                        connectPacket.getClientId(),
                        session,
                        connectPacket.isCleanSession(),
                        connectPacket.getKeepAlive()));
            } else {
                throw new IllegalStateException("Expecting the first packet from a client to be of type CONNECT.");
            }
        }
    }
}
