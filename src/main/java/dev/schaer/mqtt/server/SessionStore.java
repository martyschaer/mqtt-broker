package dev.schaer.mqtt.server;

import dev.schaer.mqtt.packets.PublishPacket;
import dev.schaer.mqtt.packets.enums.QoS;
import dev.schaer.mqtt.topic.TopicMatcher;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class SessionStore {
    private static SessionStore INSTANCE;
    private static final Logger logger = LogManager.getLogger(SessionStore.class);

    public static SessionStore onlyInstance() {
        if (INSTANCE == null) {
            INSTANCE = new SessionStore();
        }
        return INSTANCE;
    }

    // clientId -> Session
    private final Map<String, Session> sessions = new HashMap<>();

    // topic filter -> [clientIds]
    private final Map<String, Set<String>> subscriptions = new HashMap<>();

    private SessionStore() {
    }

    public void addSession(final Session session) throws IOException {
        // TODO handle cleanSession = 0
        Session existing = sessions.put(session.getClientId(), session);
        if (existing != null) {
            existing.disconnect();
        }
    }

    public void removeSession(final Session session) {
        sessions.remove(session.getClientId());
    }

    public QoS addSubscription(final String topicFilter, final String clientId) {
        subscriptions.computeIfAbsent(topicFilter, k -> new HashSet<>()).add(clientId);

        // TODO support QoS more than 0;
        return QoS.AT_MOST_ONCE;
    }

    public void removeSubscription(final String topicFilter, final String clientId) {
        Set<String> clients = subscriptions.get(topicFilter);
        if (clients != null) {
            clients.remove(clientId);
            if (clients.isEmpty()) {
                subscriptions.remove(topicFilter);
            }
        }
    }

    public void publish(PublishPacket packet) {
        for (String filter : subscriptions.keySet()) {
            if (TopicMatcher.matches(filter, packet.getTopic())) {
                for (String clientId : subscriptions.get(filter)) {
                    try {
                        sessions.get(clientId).publish(packet);
                    } catch (IOException e) {
                        logger.error("Couldn't publish packet to client " + clientId, e);
                    }
                }
            }
        }
    }
}
