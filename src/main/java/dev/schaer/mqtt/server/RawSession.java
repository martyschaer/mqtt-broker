package dev.schaer.mqtt.server;

import dev.schaer.mqtt.packets.LengthEncoding;
import dev.schaer.mqtt.packets.Packet;
import dev.schaer.mqtt.packets.PacketParser;
import dev.schaer.mqtt.packets.enums.Type;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

public class RawSession {
    private final Socket socket;
    private final InputStream input;
    private final OutputStream output;

    public RawSession(final Socket socket) throws IOException {
        this.socket = socket;
        this.input = socket.getInputStream();
        this.output = socket.getOutputStream();
    }

    /**
     * Returns the next {@link Packet} received from this {@link RawSession}.
     * This method blocks until an entire {@link Packet} is available.
     */
    public Packet receive() throws IOException {
        try {
            byte[] bytes = input.readNBytes(1);
            if (bytes.length < 1) {
                socket.close();
                return null;
            }
            byte flags = bytes[0];
            Type type = Type.parse(flags);
            int length = LengthEncoding.decode(input);
            return PacketParser.parse(type, flags, input.readNBytes(length));
        } catch (Exception e) {
            socket.close();
            throw new IllegalStateException("Something went wrong during packet parsing. Connection closed.", e);
        }
    }

    /**
     * Sends the given {@link Packet} over this {@link RawSession}.
     */
    public void send(final Packet packet) throws IOException {
        output.write(packet.serialize());
        output.flush();
    }

    public void close() throws IOException {
        socket.close();
    }

    public boolean isClosed() {
        return socket.isClosed();
    }
}
