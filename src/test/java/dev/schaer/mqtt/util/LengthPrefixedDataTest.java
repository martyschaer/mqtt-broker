package dev.schaer.mqtt.util;

import org.junit.jupiter.api.Test;

import java.nio.charset.StandardCharsets;

import static org.junit.jupiter.api.Assertions.assertEquals;

class LengthPrefixedDataTest {
    @Test
    public void testReadString() {
        byte[] raw = "____\u0000\u0004MQTT___".getBytes(StandardCharsets.UTF_8);
        int ptr = 4;
        LengthPrefixedData actual = LengthPrefixedData.read(raw, ptr);
        assertEquals("MQTT", actual.getString());
        assertEquals(10, actual.getNewPtr());
    }

    @Test
    public void testReadStringWithEmoji() {
        byte[] raw = "____\u0000\u0008MQTT\uD83D\uDE01___".getBytes(StandardCharsets.UTF_8);
        int ptr = 4;
        LengthPrefixedData actual = LengthPrefixedData.read(raw, ptr);
        assertEquals("MQTT\uD83D\uDE01", actual.getString());
        assertEquals(14, actual.getNewPtr());
    }
}