package dev.schaer.mqtt.util;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class TwoByteIntTest {

    @Test
    void shouldReadTwoByteInt8() {
        assertEquals(8, TwoByteInt.read(new byte[]{0x00, 0x08}, 0));
    }

    @Test
    void shouldReadTwoByteInt65535() {
        assertEquals(65535, TwoByteInt.read(new byte[]{(byte) 0xFFFF, (byte) 0xFFFF}, 0));
    }
}