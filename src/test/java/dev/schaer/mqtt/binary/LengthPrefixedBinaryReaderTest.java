package dev.schaer.mqtt.binary;

import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class LengthPrefixedBinaryReaderTest {
    @Test
    public void readHello() throws IOException {
        InputStream input = new ByteArrayInputStream(new byte[]{0x00, 0x05, 'H', 'e', 'l', 'l', 'o'});
        LengthPrefixedBinaryReader reader = new LengthPrefixedBinaryReader(input);
        assertEquals("Hello", reader.readString());
    }

    @Test
    public void readTooShortStream() throws IOException {
        InputStream input = new ByteArrayInputStream(new byte[]{0x0F, 0x05, 'H', 'e', 'l', 'l', 'o'});
        LengthPrefixedBinaryReader reader = new LengthPrefixedBinaryReader(input);
        assertThrows(AssertionError.class, reader::read);
    }

    @Test
    public void readMultiple() throws IOException {
        InputStream input = new ByteArrayInputStream(new byte[]{ //
                0x00, 0x05, 'H', 'e', 'l', 'l', 'o', //
                0x00, 0x06, 'W', 'o', 'r', 'l', 'd', '!' //
        });
        LengthPrefixedBinaryReader reader = new LengthPrefixedBinaryReader(input);
        assertEquals("Hello", reader.readString());
        assertEquals("World!", reader.readString());
    }
}