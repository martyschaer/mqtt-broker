package dev.schaer.mqtt.binary;

import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class TwoByteNumberBinaryReaderTest {
    @Test
    public void testReadNominal() throws IOException {
        InputStream input = new ByteArrayInputStream(new byte[]{0x05, '9'}); // 0x539
        TwoByteNumberBinaryReader reader = new TwoByteNumberBinaryReader(input);
        assertEquals(1337, reader.read());
    }

    @Test
    public void readShortStream() throws IOException {
        InputStream input = new ByteArrayInputStream(new byte[]{0x05}); // 0x5
        TwoByteNumberBinaryReader reader = new TwoByteNumberBinaryReader(input);
        assertThrows(AssertionError.class, reader::read);
    }
}