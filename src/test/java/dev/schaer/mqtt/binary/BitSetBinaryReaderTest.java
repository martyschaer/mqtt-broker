package dev.schaer.mqtt.binary;

import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.BitSet;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class BitSetBinaryReaderTest {
    @Test
    public void readBitSet() throws IOException {
        final InputStream input = new ByteArrayInputStream(new byte[]{';'}); // 00111011
        final BitSetBinaryReader reader = new BitSetBinaryReader(input);
        final BitSet actual = reader.read();
        assertFalse(actual.get(7));
        assertFalse(actual.get(6));
        assertTrue(actual.get(5));
        assertTrue(actual.get(4));
        assertTrue(actual.get(3));
        assertFalse(actual.get(2));
        assertTrue(actual.get(1));
        assertTrue(actual.get(0));
    }
}