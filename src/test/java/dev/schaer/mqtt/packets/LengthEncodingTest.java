package dev.schaer.mqtt.packets;

import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;

import static org.junit.jupiter.api.Assertions.*;

class LengthEncodingTest {
    @Test
    public void testEncoding10() throws Exception {
        byte[] expected = new byte[]{(byte) 0xA};
        assertArrayEquals(expected, LengthEncoding.encode(10));
    }

    @Test
    public void testEncoding2097151() throws Exception {
        byte[] expected = new byte[]{(byte) 0xFF, (byte) 0xFF, (byte) 0x7F};
        assertArrayEquals(expected, LengthEncoding.encode(2_097_151));
    }

    @Test
    public void testEncodingMax() throws Exception {
        byte[] expected = new byte[]{(byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0x7F};
        assertArrayEquals(expected, LengthEncoding.encode(268_435_455));
    }

    @Test
    public void testEncodingTooLarge() throws Exception {
        assertThrows(IllegalArgumentException.class,
                () -> LengthEncoding.encode(268_435_456)
        );
    }

    @Test
    public void testDecoding10() throws Exception {
        ByteArrayInputStream input = genInputStream((byte) 0xA, (byte) 0x12);
        assertEquals(10, LengthEncoding.decode(input));
        assertEquals(0x12, input.read()); // the bytes following the variable length should be left over
    }

    @Test
    public void testDecoding2097151() throws Exception {
        ByteArrayInputStream input = genInputStream((byte) 0xFF, (byte) 0xFF, (byte) 0x7F, (byte) 0x12);
        assertEquals(2_097_151, LengthEncoding.decode(input));
        assertEquals(0x12, input.read());
    }

    @Test
    public void testDecodingMax() throws Exception {
        ByteArrayInputStream input = genInputStream((byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0x7F, (byte) 0x12);
        assertEquals(268_435_455, LengthEncoding.decode(input));
        assertEquals(0x12, input.read());
    }

    @Test
    public void testDecodingTooLarge() throws Exception {
        assertThrows(IllegalArgumentException.class,
                () -> LengthEncoding.decode(genInputStream((byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0x12))
        );
    }

    private ByteArrayInputStream genInputStream(byte... bytes) {
        return new ByteArrayInputStream(bytes);
    }
}