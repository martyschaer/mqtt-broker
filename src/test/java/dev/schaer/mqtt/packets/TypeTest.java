package dev.schaer.mqtt.packets;

import dev.schaer.mqtt.packets.enums.Type;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TypeTest {
    @Test
    public void testParseReserved1() {
        assertEquals(Type.Reserved1, Type.parse(0x00));
    }

    @Test
    public void testParsePUBACK() {
        assertEquals(Type.PUBACK, Type.parse(0x40));
    }
}