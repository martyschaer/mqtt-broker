package dev.schaer.mqtt.topic;

import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

public class TopicMatcherTest {

    @Test
    public void shouldMatchExactly() {
        new TestCase("sports/tennis/player1/ranking")
                .matches("sports/tennis/player1/ranking")
                .unmatches("SPORTS/tennis/player1/ranking")
                .test();
    }

    @Test
    public void shouldMatchEverything() {
        new TestCase("#")
                .matches("sports/tennis/player1/ranking")
                .matches("///hello_world")
                .matches("/")
                .matches("z̳̩͍͖̜̤̬a̩͓̪̻͎͎̮ͫ̂̃̆̔͛l̪͐̈́ͮ̏̈́̊͆g̝͖͕̫͌ͫ̓͂͡ō͑")
                .matches("tiere/vögel/rabe/")
                .test();
    }

    @Test
    public void shouldMatchMultiLevelWildCard() {
        // example from the MQTT v3.1.1 specification
        new TestCase("sport/tennis/player1/#")
                .matches("sport/tennis/player1")
                .matches("sport/tennis/player1/ranking")
                .matches("sport/tennis/player1/score/wimbledon")
                .matches("sport/tennis/player1/")
                .unmatches("sport/baseball/player1/ranking")
                .unmatches("sport/tennis/player")
                .test();

    }

    @Test
    public void shouldMatchSingleLevelWildCard1() {
        new TestCase("sport/tennis/+")
                .matches("sport/tennis/player1")
                .matches("sport/tennis/player2")
                .unmatches("sport/tennis/player1/ranking")
                .test();
    }

    @Test
    public void shouldMatchSingleLevelWildCard2() {
        new TestCase("sport/+")
                .matches("sport/")
                .matches("sport/tennis")
                .matches("sport/football")
                .unmatches("sport")
                .unmatches("SPORT/")
                .test();
    }

    @Test
    public void shouldMatchSingleLevelWildCard3() {
        new TestCase("+/+").matches("/finance").test();
        new TestCase("/+").matches("/finance").test();
        new TestCase("+").unmatches("/finance").test();
    }

    @Test
    public void shouldMatchSingleLevelWildCard4() {
        new TestCase("sport/tennis/+/ranking")
                .matches("sport/tennis/player1/ranking")
                .matches("sport/tennis/player2/ranking")
                .matches("sport/tennis//ranking")
                .unmatches("sport/tennis/player1/score")
                .unmatches("sport/tennis/player2/score")
                .unmatches("sport/tennis")
                .test();
    }


    @Test
    public void shouldThrowInvalidTopicFilterException_whenInvalidTopicFilter() {
        assertInvalidTopicFilter("sport/tennis#");
        assertInvalidTopicFilter("sport/tennis/#/ranking");
        assertInvalidTopicFilter("sport+");
        assertInvalidTopicFilter("sport+/");
        assertInvalidTopicFilter("sport/#tennis");
        assertInvalidTopicFilter("sport/te#is");
        assertInvalidTopicFilter("sport/+tennis");
        assertInvalidTopicFilter("sport/te+is");
    }

    private void assertInvalidTopicFilter(final String invalidTopicFilter) {
        assertThrows(InvalidTopicFilterException.class,
                () -> TopicMatcher.matches(invalidTopicFilter, ""),
                "'" + invalidTopicFilter + "' should throw " + InvalidTopicFilterException.class.getSimpleName());
    }

    private static class TestCase {
        private final String filter;
        private final Set<String> matches = new HashSet<>();
        private final Set<String> unmatches = new HashSet<>();

        TestCase(final String filter) {
            this.filter = filter;
        }

        TestCase matches(final String topic) {
            this.matches.add(topic);
            return this;
        }

        TestCase unmatches(final String topic) {
            this.unmatches.add(topic);
            return this;
        }

        void test() {
            for (String shouldMatch : matches) {
                assertTrue(TopicMatcher.matches(filter, shouldMatch),
                        String.format("%s should match %s", filter, shouldMatch));
            }

            for (String shouldNotMatch : unmatches) {
                assertFalse(TopicMatcher.matches(filter, shouldNotMatch),
                        String.format("%s should NOT match %s", filter, shouldNotMatch));
            }
        }
    }

}